package com.immocloud.bewerber.immocloudbewerberbe;

public enum Status {
    OPEN,
    ACCEPTED,
    DECLINED
}
