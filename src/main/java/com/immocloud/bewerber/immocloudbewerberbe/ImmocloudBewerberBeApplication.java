package com.immocloud.bewerber.immocloudbewerberbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImmocloudBewerberBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImmocloudBewerberBeApplication.class, args);
    }

}
