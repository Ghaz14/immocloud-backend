# immocloud-bewerber

Dies ist das Backend für die Aufgabe. Es wurde mit SpringBoot mit allen erforderlichen Funktionen gemäß Anleitung erstellt.
Es umfasst die Endpunkte zum Abrufen und Hinzufügen von „Bewerber“ sowie den Endpunkt zum Aktualisieren des Status eines bestimmten Bewerbers.
Das Frontend befindet sich in einem eigenen Repository. Um das Frontend zu überprüfen, besuchen Sie bitte [hier](https://gitlab.com/Ghaz14/immocloud-frontend)


Da ich darauf aufmerksam geworden bin, dass das Backend bei immocloud in Java ist, habe ich mich entschieden, das in der Aufgabe erwähnte Java-Framework zu verwenden, um so nah wie möglich an dem zu sein, was tatsächlich bei immocloud verwendet wird.
Bitte bedenken Sie, dass es das erste Mal ist, dass ich etwas in Java geschrieben habe. Ich musste so ziemlich alles nachschlagen und recherchieren, wie man die erforderlichen Endpunkte erstellt. Bitte entschuldigen Sie meine Fehler, falls ich mich nicht an die Best Practices gehalten habe.

## Um das Projekt zu starten:

Die Hauptklasse zum Starten der Anwendung ist `ImmocloudBewerberBeApplication`