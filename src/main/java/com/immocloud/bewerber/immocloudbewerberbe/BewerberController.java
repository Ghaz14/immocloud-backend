package com.immocloud.bewerber.immocloudbewerberbe;

import ch.qos.logback.core.rolling.helper.IntegerTokenConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:5173/",methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
@RestController()
public class BewerberController {
    private final LinkedHashMap<Integer, Bewerber> bewerberList;
    private final ObjectMapper objectMapper;

    @Autowired
    public BewerberController(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        this.bewerberList = new LinkedHashMap<>();
    }

    @GetMapping(path = "/get-bewerber", produces= MediaType.APPLICATION_JSON_VALUE)
    public Collection<Bewerber> getBewerber() {
        return bewerberList.values();
    }

    @PostMapping(value = "/add-bewerber", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Bewerber addBewerber(@RequestBody String bewerber) throws JsonProcessingException {
        Bewerber myBewerber = objectMapper.readValue(bewerber, Bewerber.class);
        myBewerber.setStatus(Status.OPEN);
        bewerberList.put(myBewerber.getId(),myBewerber);
        return myBewerber;
    }

    @PutMapping(value = "/update-bewerber-status", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Bewerber updateBewerberStatus(@RequestBody String bewerber) throws JsonProcessingException {
        Bewerber myBewerber = objectMapper.readValue(bewerber, Bewerber.class);
        myBewerber.setStatus(myBewerber.getStatus());
        bewerberList.put(myBewerber.getId(), myBewerber);
        return myBewerber;
    }
}