package com.immocloud.bewerber.immocloudbewerberbe;

public class Bewerber {

    private final int id;
    private final String name;
    private Status status;

    private static int idCounter = 1;

    public Bewerber(String name, Status status) {
        this.id = idCounter++; // sync there is no database in this demo, I just put a counter fpr the id
        this.name = name;
        this.status = status;
    }

    public int getId() { return id;}

    public String getName() {
        return name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
